--This demo contains two web Applications of a report being read from the database.
--The Applications are available online as follows:

1. AngularJS + PHP + MySQL DB (Hosted on AWS, Apache web server, Windows 2012 R2)
   Link: http://ec2-52-41-129-236.us-west-2.compute.amazonaws.com/index.html#/

2. Oracle APEX + Oracle DB:
   Link: https://apex.oracle.com/pls/apex/f?p=43618:1
   Credentials: demo_manulife/dmeo