@Author: Haytham Alzeini
@Demo: https://apex.oracle.com/pls/apex/f?p=43618:1
@Credentials: demo_manulife/dmeo

@Requirements:
//1. Free Oracle APEX account in the cloud from here  https://apex.oracle.com/en/

//Alternatively:
//1. Oracle DB
//2. Oracle APEX

@Build Steps:
//If you have Oracle DB, you can install Oracle APEX on premise from here : https://apex.oracle.com/en/ (Download on OTN)
//Alternatively, you can obtain free APEX account in the cloud: https://apex.oracle.com/en/ (Free workspace)
//Once logged in to your APEX account, create Application by importing the file "f43618.sql"
//Then in SQL Workshop, run the SQL statements in the file "DDL+DML.sql"

@DONE
https://Hawk707@bitbucket.org/Hawk707/ml-demo.git