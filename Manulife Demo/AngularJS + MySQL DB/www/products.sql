CREATE DATABASE angularcode_products;


USE angularcode_products;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `angularcode_products`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--


CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` int(50) ,
  `name` varchar(100) NOT NULL,
  `price` double ,
  `mrp` double ,
  `description` varchar(500) ,
  `packing` varchar(50) ,
  `image` varchar(200) ,
  `category` int(11),
  `stock` int(11) NOT NULL,
  `status` varchar(10) ,
  `w_date` date,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=951 ;


insert into products values ( 1,null, 'www.bing.com',null, null, null, null, null, null, 14065457, null,'2013-01-06');
insert into products values ( 2,null, 'www.ebay.com.au',null, null, null, null, null, null, 19831166, null,'2013-01-06');
insert into products values ( 3,null, 'www.facebook.com',null, null, null, null, null, null, 104346720, null,'2013-01-06');
insert into products values ( 4,null, 'mail.live.com',null, null, null, null, null, null, 21536612, null,'2013-01-06');
insert into products values ( 5,null, 'www.wikipedia.org',null, null, null, null, null, null, 13246531, null,'2013-01-06');
insert into products values ( 6,null, 'www.ebay.com.au',null, null, null, null, null, null, 23154653, null,'2013-01-27');
insert into products values ( 7,null, 'au.yahoo.com',null, null, null, null, null, null, 11492756, null,'2013-01-06');
insert into products values ( 8,null, 'www.google.com',null, null, null, null, null, null, 26165099, null,'2013-01-06');
insert into products values ( 9,null, 'www.youtube.com',null, null, null, null, null, null, 68487810, null,'2013-01-13');
insert into products values ( 10,null, 'www.wikipedia.org',null, null, null, null, null, null, 16550230, null,'2013-01-27');
insert into products values ( 11,null, 'ninemsn.com.au',null, null, null, null, null, null, 21734381, null,'2013-01-06');
insert into products values ( 12,null, 'mail.live.com',null, null, null, null, null, null, 24344783, null,'2013-01-20');
insert into products values ( 13,null, 'www.ebay.com.au',null, null, null, null, null, null, 22598506, null,'2013-01-20');
insert into products values ( 14,null, 'mail.live.com',null, null, null, null, null, null, 24272437, null,'2013-01-27');
insert into products values ( 15,null, 'www.bing.com',null, null, null, null, null, null, 16041776, null,'2013-01-27');
insert into products values ( 16,null, 'ninemsn.com.au',null, null, null, null, null, null, 24241574, null,'2013-01-20');
insert into products values ( 17,null, 'www.facebook.com',null, null, null, null, null, null, 118984483, null,'2013-01-20');
insert into products values ( 18,null, 'ninemsn.com.au',null, null, null, null, null, null, 24521168, null,'2013-01-27');
insert into products values ( 19,null, 'www.facebook.com',null, null, null, null, null, null, 123831275, null,'2013-01-27');
insert into products values ( 20,null, 'www.bing.com',null, null, null, null, null, null, 16595739, null,'2013-01-20');
insert into products values ( 21,null, 'www.facebook.com',null, null, null, null, null, null, 118506019, null,'2013-01-13');
insert into products values ( 22,null, 'www.google.com.au',null, null, null, null, null, null, 170020924, null,'2013-01-20');
insert into products values ( 23,null, 'www.youtube.com',null, null, null, null, null, null, 69327140, null,'2013-01-27');
insert into products values ( 24,null, 'mail.live.com',null, null, null, null, null, null, 24772355, null,'2013-01-13');
insert into products values ( 25,null, 'ninemsn.com.au',null, null, null, null, null, null, 24555033, null,'2013-01-13');
insert into products values ( 26,null, 'www.google.com',null, null, null, null, null, null, 28996455, null,'2013-01-20');
insert into products values ( 27,null, 'www.bing.com',null, null, null, null, null, null, 16618315, null,'2013-01-13');
insert into products values ( 28,null, 'www.google.com.au',null, null, null, null, null, null, 171842376, null,'2013-01-27');
insert into products values ( 29,null, 'www.youtube.com',null, null, null, null, null, null, 59811438, null,'2013-01-06');
insert into products values ( 30,null, 'www.netbank.commbank.com.au',null, null, null, null, null, null, 13316233, null, '2013-01-13');
insert into products values ( 31,null, 'www.netbank.commbank.com.au',null, null, null, null, null, null, 13072234, null,'2013-01-20');
insert into products values ( 32,null, 'www.ebay.com.au',null, null, null, null, null, null, 22785028, null,'2013-01-13');
insert into products values ( 33,null, 'www.wikipedia.org',null, null, null, null, null, null, 16519992, null,'2013-01-20');
insert into products values ( 34,null, 'www.bom.gov.au',null, null, null, null, null, null, 14369775, null,'2013-01-27');
insert into products values ( 35,null, 'www.google.com',null, null, null, null, null, null, 29422150, null,'2013-01-27');
insert into products values ( 36,null, 'www.youtube.com',null, null, null, null, null, null, 69064107, null,'2013-01-20');
insert into products values ( 37,null, 'www.google.com.au',null, null, null, null, null, null, 151749278, null,'2013-01-06');
insert into products values ( 38,null, 'www.wikipedia.org',null, null, null, null, null, null, 16015926, null,'2013-01-13');
insert into products values ( 39,null, 'www.google.com',null, null, null, null, null, null, 29203671, null,'2013-01-13');
insert into products values ( 40,null, 'www.google.com.au',null, null, null, null, null, null, 172220397, null,'2013-01-13');



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

