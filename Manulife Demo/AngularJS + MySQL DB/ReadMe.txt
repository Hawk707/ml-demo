@Author: Haytham Alzeini
@Demo: http://ec2-52-41-129-236.us-west-2.compute.amazonaws.com/index.html#/


@Requirements:
//AWS account (1 year free)

@Build Steps:
//1. Create EC2 instance on AWS (The current instance is Windows Server 2012 R2)
//2. Set Security gruop with the rules Inbound (port: 80) allowed
//3. Install Webserver (You can use IIS shipped with Windows Server.). The current package is Wamp Server with Apache 2.4.17
//4. Configure the file httpd.conf with the following 


//a. Change <Directory .... > to be like this: 

<Directory "C:/wamp64/www/">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride all
    Require all granted
</Directory>

//where my index.html file is under www apache default directory


//b.And this is where I was stuck: In the same file, search for #   onlineoffline tag - don't remove, 
//below this line you'll find Require local. Change it to Require all granted.

//5. Copy all files into www default directory (apache server), and restart the server

//6. Use public DNS provided by AWS when created EC2 instance, to access the website from the internet

@DONE